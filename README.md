# helloworld_java

## 介绍
1. web访问输出hello world,
2. 并可获取到服务器ip地址和主机名
3. 屏幕不停的输出1000内随机数
4. 适合用jar包做实验，例如用作docker,kubernetes的简单demo使用,以及zabbix监控可获取日志中的随机数用在监控项
5. 本项目也支持gitlab-ci,用于集成gitlab和kubernetes，详情可参考博客[https://www.cnblogs.com/Sunzz/p/13716477.html](https://www.cnblogs.com/Sunzz/p/13716477.html)
## 软件架构

Spring Boot


## 安装教程
#### 下载并运行
1.  git clone https://gitee.com/SunHarvey/helloworld_java.git
2.  cd helloworld_java
3.  java -jar ./target/helloworld-1.0.jar
#### 或者自己打包后运行
1.  git clone https://gitee.com/SunHarvey/helloworld_java.git
2.  cd helloworld_java
3.  mvn clean install
4.  java -jar ./target/helloworld-1.0.jar

## 使用说明

1. 启动方式  
    ![启动](https://images.gitee.com/uploads/images/2020/0820/122730_1ff91851_4862831.png "屏幕截图.png")

2.  terminal访问
    ![terminal访问](https://images.gitee.com/uploads/images/2020/0820/114144_2d5bf45f_4862831.png "屏幕截图.png")

3.  访问helloworld
    ![访问helloworld](https://images.gitee.com/uploads/images/2020/0820/113813_adf385c9_4862831.png "屏幕截图.png")

4.  获取服务器ip和主机名
    ![获取服务器ip和主机名](https://images.gitee.com/uploads/images/2020/0820/114021_720761c8_4862831.png "屏幕截图.png")


5.  日志目录为logs


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
