FROM registry.cn-hongkong.aliyuncs.com/public-all/jdk8:v4
MAINTAINER SunHarvey
ENV USER centos
ENV APP_HOME /home/$USER/apps/
ADD target/helloworld-1.0.jar $APP_HOME
ENTRYPOINT ["java","-Dfile.encoding=UTF-8", "-jar", "helloworld-1.0.jar"]